# Obraz bazowy
FROM python:3.11-slim

# Zaktualizuj pip i zainstaluj Poetry
RUN pip install --upgrade pip && \
    pip install poetry

# Ustaw katalog roboczy w kontenerze
WORKDIR /code

# Kopiuj pliki do katalogu roboczego
COPY . /code/

# Wyłącz tworzenie wirtualnego środowiska przez Poetry i zainstaluj zależności
RUN poetry config virtualenvs.create false && \
    poetry install

# Komenda uruchamiająca trening
CMD ["poetry", "run", "python", "cicd_example/train_model.py"]