import joblib
from pathlib import Path


project_root = Path(__file__).resolve().parent.parent
MODEL_PATH = project_root / "models/naive_bayes_model.joblib"

def load_model(filename):
    return joblib.load(filename)

def predict(model, new_data):
    return model.predict(new_data)

def main():
    model = load_model(MODEL_PATH)
    X_new = [[5.1, 3.5, 1.4, 0.2], [6.7, 3.0, 5.2, 2.3]]
    predictions = predict(model, X_new)
    print(f"Predictions: {predictions}")

if __name__ == "__main__":
    main()
