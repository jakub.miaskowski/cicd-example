from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score
import joblib
from pathlib import Path

project_root = Path(__file__).resolve().parent.parent
MODEL_PATH = project_root / "models/naive_bayes_model.joblib"

def load_dataset():
    data = load_iris()
    X, y = data.data, data.target
    return train_test_split(X, y, test_size=0.2, random_state=42)

def train_model(X_train, y_train):
    model = GaussianNB()
    model.fit(X_train, y_train)
    return model

def save_model(model, filename):
    joblib.dump(model, filename)

def main():
    X_train, X_test, y_train, y_test = load_dataset()
    model = train_model(X_train, y_train)
    save_model(model, MODEL_PATH)
    
    # For demonstration, let's calculate and print the accuracy here as well
    y_pred = model.predict(X_test)
    accuracy = accuracy_score(y_test, y_pred)
    print(f"Accuracy: {accuracy}")

if __name__ == "__main__":
    main()
